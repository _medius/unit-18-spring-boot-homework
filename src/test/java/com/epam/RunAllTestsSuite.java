package com.epam;

import com.epam.controller.TestOrderController;
import com.epam.integration.TestByRestTemplate;
import com.epam.integration.TestBySpringBootTest;
import com.epam.repository.TestOrderRepository;
import com.epam.service.TestNegativeOrderService;
import com.epam.service.TestPositiveOrderService;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestOrderController.class,
		TestPositiveOrderService.class,
		TestNegativeOrderService.class,
		TestOrderRepository.class,
		TestByRestTemplate.class,
		TestBySpringBootTest.class})
public class RunAllTestsSuite {
}
