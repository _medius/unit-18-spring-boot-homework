package com.epam;

import com.epam.domain.*;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import java.time.LocalDate;
import java.util.Arrays;

@TestConfiguration
public class TestBeansConfig {

	@Bean
	@ConfigurationProperties(prefix = "test-order")
	public Order testOrder() {
		Manufacturer manufacturerOne = new Manufacturer();
		manufacturerOne.setCity("TestCityOne");
		manufacturerOne.setCountry("TestCountryOne");
		manufacturerOne.setCompanyName("TestCompanyOne");

		Dress dressOne = new Dress();
		dressOne.setColor("TestColorOne");
		dressOne.setSize(41);
		dressOne.setManufacturer(manufacturerOne);

		Manufacturer manufacturerTwo = new Manufacturer();
		manufacturerTwo.setCity("TestCityTwo");
		manufacturerTwo.setCountry("TestCountryTwo");
		manufacturerTwo.setCompanyName("TestCompanyTwo");

		Dress dressTwo = new Dress();
		dressTwo.setColor("TestColorTwo");
		dressTwo.setSize(42);
		dressTwo.setManufacturer(manufacturerTwo);

		BillingDetails billingDetails = new BillingDetails();
		billingDetails.setCardNumber(987654321);
		billingDetails.setPaymentSystem(PaymentSystem.VISA);

		Customer customer = new Customer();
		customer.setFirstName("TestName");
		customer.setLastName("TestSurname");
		customer.setBillingDetails(billingDetails);

		Order order = new Order();
		order.setCustomer(customer);
		order.setDate(LocalDate.of(2007, 4,18));
		order.setDresses(Arrays.asList(dressOne, dressTwo));
		return order;
	}

	@Bean
	@ConfigurationProperties(prefix = "test-get-order")
	public Order testGetOrder() {
		Manufacturer manufacturerOne = new Manufacturer();
		manufacturerOne.setId(1L);
		manufacturerOne.setCity("Samara");
		manufacturerOne.setCountry("Russia");
		manufacturerOne.setCompanyName("AK");

		Dress dressOne = new Dress();
		dressOne.setId(3L);
		dressOne.setColor("Blue");
		dressOne.setSize(46);
		dressOne.setManufacturer(manufacturerOne);

		Manufacturer manufacturerTwo = new Manufacturer();
		manufacturerTwo.setId(2L);
		manufacturerTwo.setCity("Berlin");
		manufacturerTwo.setCountry("Germany");
		manufacturerTwo.setCompanyName("DR");

		Dress dressTwo = new Dress();
		dressTwo.setId(6L);
		dressTwo.setColor("Green");
		dressTwo.setSize(38);
		dressTwo.setManufacturer(manufacturerTwo);

		BillingDetails billingDetails = new BillingDetails();
		billingDetails.setId(2L);
		billingDetails.setCardNumber(222222222);
		billingDetails.setPaymentSystem(PaymentSystem.MASTERCARD);

		Customer customer = new Customer();
		customer.setId(2L);
		customer.setFirstName("Jesse");
		customer.setLastName("Pinkman");
		customer.setBillingDetails(billingDetails);

		Order order = new Order();
		order.setId(2L);
		order.setCustomer(customer);
		order.setDate(LocalDate.of(2019, 5,19));
		order.setDresses(Arrays.asList(dressOne, dressTwo));
		return order;
	}
}