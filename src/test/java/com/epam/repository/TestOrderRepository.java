package com.epam.repository;

import com.epam.TestBeansConfig;
import com.epam.domain.Order;
import com.epam.repos.interfaces.OrderRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@Import(TestBeansConfig.class)
@DataJpaTest
public class TestOrderRepository {

	private long testId;

	private static final String GET_ALL_ORDERS = "FROM Order";
	private static final int FIRST_ELEMENT = 0;
	private static final String CUSTOMER_LASTNAME = "White";

	private static final LocalDate FIRST_TEST_DATE = LocalDate.of(2019, Month.MARCH, 1);
	private static final LocalDate SECOND_TEST_DATE = LocalDate.of(2019, Month.APRIL, 1);
	private static final LocalDate THIRD_TEST_DATE = LocalDate.of(2019, Month.MAY, 1);

	private boolean afterSave = false;
	private boolean afterDelete = false;

	private Order tempOrder;

	@Autowired
	private Order testOrder;

	@Autowired
	private TestEntityManager testEntityManager;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private OrderRepository repository;

	@Before
	public void setUp() {
		testId = entityManager
				.createQuery(GET_ALL_ORDERS, Order.class)
				.getResultList()
				.get(FIRST_ELEMENT)
				.getId();
	}

	@After
	public void tearDown() {
		if (afterSave) {
			entityManager.remove(testOrder);

			testOrder.setId(null);
			testOrder.getCustomer().setId(null);
			testOrder.getCustomer().getBillingDetails().setId(null);
			testOrder.getDresses().forEach(dress -> {
				dress.setId(null);
				dress.getManufacturer().setId(null);
			});
		}

		if (afterDelete) {
			entityManager.persist(tempOrder);
		}
	}

	@Test
	public void save() {
		repository.save(testOrder);
		afterSave = true;
		assertEquals(testOrder, testEntityManager.find(Order.class, testOrder.getId()));
	}

	@Test
	public void findAll() {
		List<Order> orderList = StreamSupport.stream(repository.findAll().spliterator(), false)
				.collect(Collectors.toList());
		assertThat(orderList).hasSize(3);
	}

	@Test
	public void findById() {
		assertEquals(testId, (long)repository.findById(testId).get().getId());
	}

	@Test
	public void deleteById() {
		tempOrder = testEntityManager.find(Order.class, testId);

		repository.deleteById(testId);
		afterDelete = true;

		assertNull(testEntityManager.find(Order.class, testId));
	}

	@Test
	public void findByDateAfter() {
		List<Order> orderList = repository.findByDateAfter(FIRST_TEST_DATE);
		assertThat(orderList).hasSize(3);

		orderList = repository.findByDateAfter(SECOND_TEST_DATE);
		assertThat(orderList).hasSize(2);

		orderList = repository.findByDateAfter(THIRD_TEST_DATE);
		assertThat(orderList).hasSize(1);
	}

	@Test
	public void findByCustomerLastName() {
		Order order = repository
				.findByCustomer_LastNameOrderByCustomer_FirstName(CUSTOMER_LASTNAME).get(FIRST_ELEMENT);
		assertEquals(CUSTOMER_LASTNAME, order.getCustomer().getLastName());
	}
}