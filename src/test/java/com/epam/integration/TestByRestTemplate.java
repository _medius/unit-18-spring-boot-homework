package com.epam.integration;

import com.epam.config.TestBeans;
import com.epam.domain.Order;
import com.epam.repos.interfaces.OrderRepository;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@ActiveProfiles(profiles = "test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TestByRestTemplate {

	private static final String GET_ALL_ORDERS = "FROM Order";

	private static final long ID_TO_TEST_NOT_FOUND = 25;
	private static final int FIRST_ELEMENT = 0;

	private Order tempOrder;
	private boolean afterDelete = false;

	@PersistenceContext(type = PersistenceContextType.EXTENDED)
	private EntityManager entityManager;

	@Autowired
	private OrderRepository repository;

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
	private TestBeans testBeans;

	@After
	public void tearDown() {
		if (afterDelete) {
			entityManager.persist(tempOrder);
		}
	}

	@Test
	public void createOrder() {
		Order testOrder = testBeans.getTestOrder();

		ResponseEntity<Order> response = restTemplate
				.postForEntity("/orders", testOrder, Order.class);

		assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
		assertThat(response.getBody().getId(), notNullValue());
		assertThat(response.getBody(), equalTo(testOrder));
	}

	@Test
	public void getOrderById() {
		Order testGetOrder = entityManager
				.createQuery(GET_ALL_ORDERS, Order.class)
				.getResultList()
				.get(FIRST_ELEMENT);

		ResponseEntity<Order> response = restTemplate
				.getForEntity("/orders/{id}", Order.class, testGetOrder.getId());

		assertThat(response.getStatusCode(), is(HttpStatus.OK));
		assertThat(response.getBody().getId(), notNullValue());
		assertThat(response.getBody(), equalTo(testGetOrder));
	}

	@Test
	public void getNotFoundById() {
		ResponseEntity<Order> response = restTemplate
				.getForEntity("/orders/{id}", Order.class, ID_TO_TEST_NOT_FOUND);

		assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
	}

	@Test
	public void getAllOrders() {
		List<Order> listCompareTo = entityManager
				.createQuery(GET_ALL_ORDERS, Order.class)
				.getResultList();

		ResponseEntity<List<Order>> response = restTemplate
				.exchange("/orders/list",
						HttpMethod.GET,
						null,
						new ParameterizedTypeReference<List<Order>>() {});
		List<Order> orderList = response.getBody();

		assertThat(response.getStatusCode(), is(HttpStatus.OK));
		assertThat(orderList).isEqualTo(listCompareTo);
	}

	@Test
	public void deleteOrder() {
		tempOrder = entityManager
				.createQuery(GET_ALL_ORDERS, Order.class)
				.getResultList()
				.get(FIRST_ELEMENT);

		afterDelete = true;

		ResponseEntity<String> response = restTemplate
				.exchange("/orders/" + tempOrder.getId(),
						HttpMethod.DELETE,
						null,
						String.class);

		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}
}
