package com.epam.integration;

import com.epam.MainApp;
import com.epam.config.TestBeans;
import com.epam.domain.Customer;
import com.epam.domain.Order;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.epam.service.interfaces.OrderService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.time.Month;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
		classes = MainApp.class,
		webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
@TestPropertySource(
		locations = "classpath:application-test.yml")
public class TestBySpringBootTest {

	private static final int LIST_SIZE_TO_FIND_BY_LASTNAME = 1;
	private static final int LIST_SIZE_TO_FIND_BY_DATE = 3;

	private static final String GET_ALL_ORDERS = "FROM Order";
	private static final int FIRST_ELEMENT = 0;

	private static final String LASTNAME_TO_TEST_NOT_FOUND = "Salamanca";
	private static final LocalDate DATE_TO_TEST_NOT_FOUND = LocalDate.of(2021, Month.MAY, 14);

	private Customer originalCustomer;
	private LocalDate originalDate;

	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private OrderService service;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private TestBeans testBeans;

	@Autowired
	private ObjectMapper objectMapper;

	@Before
	public void setUp() {
		originalCustomer = testBeans.getTestOrder().getCustomer();
		originalDate = testBeans.getTestOrder().getDate();
	}

	@After
	public void tearDown() {
		testBeans.getTestOrder().setCustomer(originalCustomer);
		testBeans.getTestOrder().setDate(originalDate);
	}

	@Test
	public void testPut() throws Exception {
		Order originalOrder = entityManager
				.createQuery(GET_ALL_ORDERS, Order.class)
				.getResultList()
				.get(FIRST_ELEMENT);

		Order testOrder = testBeans.getTestOrder();

		mockMvc.perform(put("/orders/{id}", originalOrder.getId())
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(testOrder)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("date", is(testOrder.getDate().toString())))
				.andExpect(jsonPath("customer.lastName", is(testOrder.getCustomer().getLastName())))
				.andExpect(jsonPath("dresses[0].color", is(testOrder.getDresses().get(FIRST_ELEMENT).getColor())));

		service.update(originalOrder, originalOrder.getId());
	}

	@Test
	public void testFindByCustomerLastName() throws Exception {
		Order testGetOrder = testBeans.getTestGetOrder();

		mockMvc.perform(get("/orders/customer?lastname=" + testGetOrder.getCustomer().getLastName()))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(LIST_SIZE_TO_FIND_BY_LASTNAME)))
				.andExpect(jsonPath("$.[0].customer.lastName", is(testGetOrder.getCustomer().getLastName())));
	}

	@Test
	public void testNotFoundByCustomerLastName() throws Exception {
		mockMvc.perform(get("/orders/customer?lastname=" + LASTNAME_TO_TEST_NOT_FOUND))
				.andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	public void testFindByDateAfter() throws Exception {
		Order testOrder = testBeans.getTestOrder();

		mockMvc.perform(get("/orders/after?date=" + testOrder.getDate()))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(LIST_SIZE_TO_FIND_BY_DATE)));
	}

	@Test
	public void testNotFoundByDateAfter() throws Exception {
		mockMvc.perform(get("/orders/after?date=" + DATE_TO_TEST_NOT_FOUND))
				.andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	public void testPostNotValidDate() throws Exception {
		Order testOrder = testBeans.getTestOrder();
		testOrder.setDate(null);

		mockMvc.perform(post("/orders")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(testOrder)))
				.andDo(print())
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testPostNotValidCustomer() throws Exception {
		Order testOrder = testBeans.getTestOrder();
		testOrder.setCustomer(null);

		mockMvc.perform(post("/orders")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(testOrder)))
				.andDo(print())
				.andExpect(status().isBadRequest());
	}
}
