package com.epam.controller;

import com.epam.TestBeansConfig;
import com.epam.domain.Order;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.epam.service.interfaces.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.List;

import static org.mockito.BDDMockito.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;

@RunWith(SpringRunner.class)
@Import(TestBeansConfig.class)
@WebMvcTest(OrderController.class)
public class TestOrderController {

	private static final int WANTED_NUMBER_OF_INVOCATIONS = 1;
	private static final long TEST_ID = 2;
	private static final int RETURNED_LIST_SIZE = 1;

	@Autowired
	private Order testOrder;

	@Autowired
	private Order testGetOrder;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private OrderService service;

	@Test
	public void getOrderById() throws Exception {

		given(service.findById(testGetOrder.getId())).willReturn(testGetOrder);

		mockMvc.perform(get("/orders/{id}", testGetOrder.getId()))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().json(objectMapper.writeValueAsString(testGetOrder)));

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(testGetOrder.getId());
	}

	@Test
	public void getAllOrders() throws Exception {

		List<Order> orderList = Collections.singletonList(testGetOrder);

		given(service.findAllOrders()).willReturn(orderList);

		mockMvc.perform(get("/orders/list"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(RETURNED_LIST_SIZE)))
				.andExpect(content().json(objectMapper.writeValueAsString(orderList)));

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS)).findAllOrders();
	}

	@Test
	public void postOrder() throws Exception {

		doNothing().when(service).save(testOrder);

		mockMvc.perform(post("/orders")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(testOrder)))
				.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().json(objectMapper.writeValueAsString(testOrder)));

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS)).save(testOrder);
	}

	@Test
	public void putOrder() throws Exception {

		given(service.update(testOrder, TEST_ID)).willReturn(testOrder);

		mockMvc.perform(put("/orders/{id}", TEST_ID)
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(objectMapper.writeValueAsString(testOrder)))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().json(objectMapper.writeValueAsString(testOrder)));

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS)).update(testOrder, TEST_ID);
	}

	@Test
	public void deleteOrder() throws Exception {

		doNothing().when(service).deleteById(TEST_ID);

		mockMvc.perform(delete("/orders/{id}", TEST_ID))
				.andDo(print())
				.andExpect(status().isNoContent());

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS)).deleteById(TEST_ID);
	}

	@Test
	public void findOrdersByCustomerSurname() throws Exception {

		List<Order> orderList = Collections.singletonList(testGetOrder);

		given(service.findByCustomerLastName(testGetOrder.getCustomer().getLastName()))
				.willReturn(orderList);

		mockMvc.perform(get("/orders/customer?lastname=" + testGetOrder.getCustomer().getLastName()))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(RETURNED_LIST_SIZE)))
				.andExpect(content().json(objectMapper.writeValueAsString(orderList)));

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS))
				.findByCustomerLastName(testGetOrder.getCustomer().getLastName());
	}

	@Test
	public void findOrdersWithDateAfter() throws Exception {

		List<Order> orderList = Collections.singletonList(testGetOrder);

		given(service.findByDateAfter(testGetOrder.getDate())).willReturn(orderList);

		mockMvc.perform(get("/orders/after?date=" + testGetOrder.getDate()))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(content().json(objectMapper.writeValueAsString(orderList)));

		verify(service, times(WANTED_NUMBER_OF_INVOCATIONS))
				.findByDateAfter(testGetOrder.getDate());
	}
}
