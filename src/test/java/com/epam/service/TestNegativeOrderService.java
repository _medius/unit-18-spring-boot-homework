package com.epam.service;

import com.epam.TestBeansConfig;
import com.epam.domain.Order;
import com.epam.error.OrderNotFoundException;
import com.epam.repos.interfaces.OrderRepository;
import com.epam.service.impl.OrderServiceImpl;
import com.epam.service.interfaces.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@Import(TestBeansConfig.class)
public class TestNegativeOrderService {

	private static final int WANTED_NUMBER_OF_INVOCATIONS = 1;
	private static final int NO_INVOCATIONS = 0;
	private static final long TEST_ID = 2;

	@Autowired
	private Order testOrder;

	@Autowired
	private Order testGetOrder;

	@Autowired
	private OrderService service;

	@MockBean
	private OrderRepository repository;

	@TestConfiguration
	static class TestOrderServiceConfig {

		@Bean
		public OrderService service() {
			return new OrderServiceImpl();
		}
	}

	@Test(expected = OrderNotFoundException.class)
	public void findById() {
		when(repository.findById(TEST_ID)).thenReturn(Optional.empty());
		service.findById(TEST_ID);
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(TEST_ID);
	}

	@Test(expected = OrderNotFoundException.class)
	public void deleteById() {
		when(repository.findById(TEST_ID)).thenReturn(Optional.empty());
		service.deleteById(TEST_ID);
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(TEST_ID);
		verify(repository, times(NO_INVOCATIONS)).deleteById(TEST_ID);
	}

	@Test(expected = OrderNotFoundException.class)
	public void update() {
		Order updatedOrder = new Order(testOrder);
		updatedOrder.setId(testGetOrder.getId());

		when(repository.findById(TEST_ID)).thenReturn(Optional.empty());
		service.update(testOrder, TEST_ID);
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(TEST_ID);
		verify(repository, times(NO_INVOCATIONS)).save(updatedOrder);
	}

	@Test(expected = OrderNotFoundException.class)
	public void findByCustomerLastName() {
		when(repository.findByCustomer_LastNameOrderByCustomer_FirstName(testOrder.getCustomer().getLastName()))
				.thenReturn(Collections.emptyList());
		service.findByCustomerLastName(testOrder.getCustomer().getLastName());
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS))
				.findByCustomer_LastNameOrderByCustomer_FirstName(testOrder.getCustomer().getLastName());
	}

	@Test(expected = OrderNotFoundException.class)
	public void findByDateAfter() {
		when(repository.findByDateAfter(testOrder.getDate())).thenReturn(Collections.emptyList());
		service.findByDateAfter(testOrder.getDate());
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS))
				.findByDateAfter(testOrder.getDate());
	}
}
