package com.epam.service;

import com.epam.TestBeansConfig;
import com.epam.domain.Order;
import com.epam.repos.interfaces.OrderRepository;
import com.epam.service.impl.OrderServiceImpl;
import com.epam.service.interfaces.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@Import(TestBeansConfig.class)
public class TestPositiveOrderService {

	private static final int WANTED_NUMBER_OF_INVOCATIONS = 1;
	private static final long TEST_ID = 2;

	@Autowired
	private Order testOrder;

	@Autowired
	private Order testGetOrder;

	@Autowired
	private OrderService service;

	@MockBean
	private OrderRepository repository;

	@TestConfiguration
	static class TestOrderServiceConfig {

		@Bean
		public OrderService service() {
			return new OrderServiceImpl();
		}
	}

	@Test
	public void findById() {
		when(repository.findById(testGetOrder.getId())).thenReturn(Optional.of(testGetOrder));
		assertEquals(testGetOrder, service.findById(testGetOrder.getId()));
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(testGetOrder.getId());
	}

	@Test
	public void findAllOrders() {
		List<Order> orderList = Collections.singletonList(testGetOrder);
		when(repository.findAll()).thenReturn(orderList);
		assertEquals(orderList, service.findAllOrders());
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS)).findAll();
	}

	@Test
	public void deleteById() {
		when(repository.findById(TEST_ID)).thenReturn(Optional.of(testGetOrder));
		doNothing().when(repository).deleteById(TEST_ID);
		service.deleteById(TEST_ID);
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(TEST_ID);
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS)).deleteById(TEST_ID);
	}

	@Test
	public void save() {
		when(repository.save(testOrder)).thenReturn(testOrder);
		service.save(testOrder);
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS)).save(testOrder);
	}

	@Test
	public void update() {
		Order updatedOrder = new Order(testOrder);
		updatedOrder.setId(testGetOrder.getId());

		when(repository.findById(TEST_ID)).thenReturn(Optional.of(testGetOrder));
		when(repository.save(updatedOrder)).thenReturn(updatedOrder);
		assertEquals(updatedOrder, service.update(testOrder, TEST_ID));
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS)).findById(TEST_ID);
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS)).save(updatedOrder);
	}

	@Test
	public void findByCustomerLastName() {
		List<Order> orderList = Collections.singletonList(testGetOrder);
		when(repository.findByCustomer_LastNameOrderByCustomer_FirstName(testOrder.getCustomer().getLastName()))
				.thenReturn(orderList);
		assertEquals(orderList, service.findByCustomerLastName(testOrder.getCustomer().getLastName()));
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS))
				.findByCustomer_LastNameOrderByCustomer_FirstName(testOrder.getCustomer().getLastName());
	}

	@Test
	public void findByDateAfter() {
		List<Order> orderList = Collections.singletonList(testGetOrder);
		when(repository.findByDateAfter(testOrder.getDate())).thenReturn(orderList);
		assertEquals(orderList, service.findByDateAfter(testOrder.getDate()));
		verify(repository, times(WANTED_NUMBER_OF_INVOCATIONS))
				.findByDateAfter(testOrder.getDate());
	}
}
