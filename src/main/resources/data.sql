INSERT INTO manufacturers (city, country, company_name) VALUES ('Samara', 'Russia', 'AK');
INSERT INTO manufacturers (city, country, company_name) VALUES ('Berlin', 'Germany', 'DR');

INSERT INTO dresses (color, size, manufacturer_id) VALUES ('White', 42, 1);
INSERT INTO dresses (color, size, manufacturer_id) VALUES ('Red', 44, 1);
INSERT INTO dresses (color, size, manufacturer_id) VALUES ('Blue', 46, 1);
INSERT INTO dresses (color, size, manufacturer_id) VALUES ('Yellow', 48, 2);
INSERT INTO dresses (color, size, manufacturer_id) VALUES ('Purple', 40, 2);
INSERT INTO dresses (color, size, manufacturer_id) VALUES ('Green', 38, 2);

INSERT INTO billing_details (card_number, payment_system) VALUES (111111111, 'AMERICAN_EXPRESS');
INSERT INTO billing_details (card_number, payment_system) VALUES (222222222, 'MASTERCARD');
INSERT INTO billing_details (card_number, payment_system) VALUES (333333333, 'VISA');

INSERT INTO customers (first_name, last_name, billing_details_id) VALUES ('Walter', 'White', 1);
INSERT INTO customers (first_name, last_name, billing_details_id) VALUES ('Jesse', 'Pinkman', 2);
INSERT INTO customers (first_name, last_name, billing_details_id) VALUES ('Jimmy', 'McGill', 3);

INSERT INTO orders (date, customer_id) VALUES ('2019-03-15', 1);
INSERT INTO orders (date, customer_id) VALUES ('2019-05-19', 2);
INSERT INTO orders (date, customer_id) VALUES ('2019-04-06', 3);

INSERT INTO orders_dresses (order_id, dress_id) VALUES (1, 1);
INSERT INTO orders_dresses (order_id, dress_id) VALUES (1, 2);
INSERT INTO orders_dresses (order_id, dress_id) VALUES (1, 4);

INSERT INTO orders_dresses (order_id, dress_id) VALUES (2, 3);
INSERT INTO orders_dresses (order_id, dress_id) VALUES (2, 6);

INSERT INTO orders_dresses (order_id, dress_id) VALUES (3, 1);
INSERT INTO orders_dresses (order_id, dress_id) VALUES (3, 3);
INSERT INTO orders_dresses (order_id, dress_id) VALUES (3, 5);
INSERT INTO orders_dresses (order_id, dress_id) VALUES (3, 6);
