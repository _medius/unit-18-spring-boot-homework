package com.epam.service.interfaces;

import com.epam.domain.Order;

import java.time.LocalDate;
import java.util.List;

public interface OrderService {
	List<Order> findAllOrders();
	Order findById(Long id);
	void deleteById(Long id);

	void save(Order order);
	Order update(Order order, Long id);

	List<Order> findByCustomerLastName(String lastName);
	List<Order> findByDateAfter(LocalDate date);
}
