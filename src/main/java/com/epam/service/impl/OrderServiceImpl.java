package com.epam.service.impl;

import com.epam.domain.Order;
import com.epam.error.OrderNotFoundException;
import com.epam.repos.interfaces.OrderRepository;
import com.epam.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Override
	public List<Order> findAllOrders() {
		return StreamSupport.stream(orderRepository.findAll().spliterator(), false)
				.collect(Collectors.toList());
	}

	@Override
	public Order findById(Long id) {
		return orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));
	}

	@Override
	public void deleteById(Long id) {
		orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));
		orderRepository.deleteById(id);
	}

	@Override
	public void save(Order order) {
		orderRepository.save(order);
	}

	@Override
	public Order update(Order order, Long id) {
		Order orderToUpdate = orderRepository.findById(id).orElseThrow(() -> new OrderNotFoundException(id));

		orderToUpdate.setDresses(order.getDresses());
		orderToUpdate.setCustomer(order.getCustomer());
		orderToUpdate.setDate(order.getDate());

		return orderRepository.save(orderToUpdate);
	}

	@Override
	public List<Order> findByCustomerLastName(String lastName) {
		List<Order> ordersList = orderRepository.findByCustomer_LastNameOrderByCustomer_FirstName(lastName);
		if (ordersList.isEmpty()) {
			throw new OrderNotFoundException("Orders of customer with surname " + lastName + " not found.");
		}
		return ordersList;
	}

	@Override
	public List<Order> findByDateAfter(LocalDate date) {
		List<Order> ordersList = orderRepository.findByDateAfter(date);
		if (ordersList.isEmpty()) {
			throw new OrderNotFoundException("Orders with date after " + date + " not found.");
		}
		return ordersList;
	}
}
