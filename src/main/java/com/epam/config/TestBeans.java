package com.epam.config;

import com.epam.domain.Order;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Setter
@Configuration
@PropertySource("classpath:application-test.yml")
@ConfigurationProperties(prefix = "testing")
public class TestBeans {
	private Order testOrder;
	private Order testGetOrder;
}
