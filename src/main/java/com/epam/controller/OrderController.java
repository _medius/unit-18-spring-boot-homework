package com.epam.controller;

import com.epam.domain.*;
import com.epam.service.interfaces.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@GetMapping("/list")
	public List<Order> getAllOrders() {
		return orderService.findAllOrders();
	}

	@GetMapping("/{id}")
	public Order getOrderById(@PathVariable Long id) {
		return orderService.findById(id);
	}

	@GetMapping("/customer")
	public List<Order> findOrdersByCustomerSurname(@RequestParam() String lastname) {
		return orderService.findByCustomerLastName(lastname);
	}

	@GetMapping("/after")
	public List<Order> findOrdersWithDateAfter(@RequestParam LocalDate date) {
		return orderService.findByDateAfter(date);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrderById(@PathVariable Long id) {
		orderService.deleteById(id);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public Order createOrder(@RequestBody @Valid Order order,
	                         HttpServletResponse response) {
		orderService.save(order);
		response.setHeader("Location", "/orders/" + order.getId() +'/');
		return order;
	}

	@PutMapping("/{id}")
	public Order updateOrder(@PathVariable Long id,
	                         @RequestBody Order order) {
		return orderService.update(order, id);
	}
}
