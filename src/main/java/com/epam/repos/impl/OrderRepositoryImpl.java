package com.epam.repos.impl;

import com.epam.domain.Order;
import com.epam.repos.interfaces.CustomRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.stream.Collectors;

@Repository
@Transactional
public class OrderRepositoryImpl implements CustomRepository<Order> {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public <S extends Order> S save(S order) {
		if (order.getCustomer().getId() == null) {
			entityManager.persist(order.getCustomer());
		} else {
			order.setCustomer(entityManager.merge(order.getCustomer()));
		}

		order.setDresses(order
				.getDresses()
				.stream()
				.map(dress -> {
					if (dress.getId() == null) {
						entityManager.persist(dress);
					} else {
						dress = entityManager.merge(dress);
					}
					return dress;
				})
				.collect(Collectors.toList()));

		if (order.getId() == null) {
			entityManager.persist(order);
		} else {
			order = entityManager.merge(order);
		}

		return order;
	}
}
