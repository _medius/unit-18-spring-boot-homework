package com.epam.repos.interfaces;

import com.epam.domain.Order;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface OrderRepository extends CrudRepository<Order, Long>, CustomRepository<Order> {

	List<Order> findByCustomer_LastNameOrderByCustomer_FirstName(String lastName);
	List<Order> findByDateAfter(LocalDate date);
}
