package com.epam.repos.interfaces;

public interface CustomRepository<T> {
	<S extends T> S save(S s);
}
