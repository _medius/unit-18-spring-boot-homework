package com.epam.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "manufacturers")
public class Manufacturer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "company_name")
	private String companyName;

	@Column
	private String country;

	@Column
	private String city;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Manufacturer)) return false;
		Manufacturer that = (Manufacturer) o;
		return Objects.equals(getCompanyName(), that.getCompanyName()) &&
				Objects.equals(getCountry(), that.getCountry()) &&
				Objects.equals(getCity(), that.getCity());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getCompanyName(), getCountry(), getCity());
	}
}
