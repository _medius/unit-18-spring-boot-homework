package com.epam.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "billing_details")
public class BillingDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private int cardNumber;

	@Enumerated(EnumType.STRING)
	@Column(name = "payment_system")
	private PaymentSystem paymentSystem;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof BillingDetails)) return false;
		BillingDetails that = (BillingDetails) o;
		return getCardNumber() == that.getCardNumber() &&
				getPaymentSystem() == that.getPaymentSystem();
	}

	@Override
	public int hashCode() {
		return Objects.hash(getCardNumber(), getPaymentSystem());
	}
}
