package com.epam.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "customers")
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private String firstName;

	@Column
	private String lastName;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "billing_details_id")
	private BillingDetails billingDetails;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Customer)) return false;
		Customer customer = (Customer) o;
		return Objects.equals(getFirstName(), customer.getFirstName()) &&
				Objects.equals(getLastName(), customer.getLastName()) &&
				Objects.equals(getBillingDetails(), customer.getBillingDetails());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getFirstName(), getLastName(), getBillingDetails());
	}
}
