package com.epam.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@Entity
@Table(name = "orders")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotNull
	@ManyToOne(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	private Customer customer;

	@NotNull
	@Column
	private LocalDate date;

	@ManyToMany(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH},
			fetch = FetchType.EAGER)
	@JoinTable( joinColumns = {@JoinColumn(name = "order_id")},
			inverseJoinColumns = {@JoinColumn(name = "dress_id")})
	private List<Dress> dresses;

	public Order(Order order) {
		this.id = order.getId();
		this.date = order.getDate();
		this.customer = order.getCustomer();
		this.dresses = order.getDresses();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Order)) return false;
		Order order = (Order) o;
		return Objects.equals(getCustomer(), order.getCustomer()) &&
				Objects.equals(getDate(), order.getDate()) &&
				Objects.equals(getDresses(), order.getDresses());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getCustomer(), getDate(), getDresses());
	}
}