package com.epam.domain;

public enum PaymentSystem {
	VISA,
	MASTERCARD,
	AMERICAN_EXPRESS;
}
