package com.epam.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Objects;

@Data
@Entity
@Table(name = "dresses")
public class Dress {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	private int size;

	@Column
	private String color;

	@ManyToOne(cascade = CascadeType.ALL)
	private Manufacturer manufacturer;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Dress)) return false;
		Dress dress = (Dress) o;
		return getSize() == dress.getSize() &&
				Objects.equals(getColor(), dress.getColor()) &&
				Objects.equals(getManufacturer(), dress.getManufacturer());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getSize(), getColor(), getManufacturer());
	}
}
