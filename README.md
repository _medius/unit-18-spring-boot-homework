# SpringBoot Homework

This is SpringBoot exercise web application which sends and receives JSON.
It is a database of dresses shop. DB scheme is in root folder of project.

- Run application via com\MainApp.java
- Run tests via com\RunAllTestsSuite.java
- Application runs on location http://localhost:1111/shop/
(if necessary port and web context can be configured on application.properties).
- REST endpoints is looks as follows (HTTP method) url:
  - get all orders
    - (GET) http://localhost:1111/shop/orders/list/
  - get order by id
    - (GET) http://localhost:1111/shop/orders/{id}/
  - get orders which date after specified
    - (GET) http://localhost:1111/shop/orders/dateAfter/
  - get orders with customer with specified lastname, order by firstname
    - (GET) http://localhost:1111/shop/orders/customer/lastname
  - delete order by id
    - (DELETE) http://localhost:1111/shop/orders/{id}/
  - update order by id
    - (PUT) http://localhost:1111/shop/orders/{id}/
  - create new order
    - (POST) http://localhost:1111/shop/orders/
  - actuator health information
    - (GET) 
- In root folder of project there is the postman collection to test endpoints.
- There is two profiles in project:
    - local (default) - Creates embedded database and fill it with test data.
    Test data contains 3 orders, 2 manufacturers, 3 payment systems and 6 dresses.
    - prod - Connects to on-disk database postgreSQL and needs to create 
    database named "shop". Port, password and username configures 
    on application-prod.properties.